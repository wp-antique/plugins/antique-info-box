(function (blocks, element, blockEditor, i18n, serverSideRender) {
    var el = element.createElement;
    var registerBlockType = blocks.registerBlockType;
    var useBlockProps = blockEditor.useBlockProps;
    var __ = i18n.__;
    var ServerSideRender = serverSideRender;

    registerBlockType('antique-info-box/block', {
        apiVersion: 2,
        title: __('Antique Info Box', 'antique-info-box'),
        icon: 'info-outline',
        category: 'widgets',
        edit: function () {
            var blockProps = useBlockProps();

            return el('div', blockProps,
                    el(ServerSideRender, {block: 'antique-info-box/block'})
                    );
        }
    });
})(
        window.wp.blocks,
        window.wp.element,
        window.wp.blockEditor,
        window.wp.i18n,
        window.wp.serverSideRender
        );

