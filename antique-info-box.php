<?php
/**
 * Plugin Name: Antique Info Box
 * Plugin URI: https://gitlab.com/wp-antique/plugins/antique-info-box
 * Description: Provide additional information about your article by inserting an information box.
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: antique-info-box
 * Domain Path: /languages
 */
/*
  Copyright 2023 Simon Garbin
  Antique Info Box is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Antique Info Box is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Antique Info Box. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
/*
  Comments:
  - Every function is prefixed by the plugin name in order to prevent
  name collisions.
  - In almost all functions named arguments are used in order to facilitate
  the understanding of the code.
  - Single quotation marks are used for php/js code, double quotation marks
  for HTML.
 */

include plugin_dir_path(__FILE__) . '/shared/menu.php';
include plugin_dir_path(__FILE__) . '/shared/register.php';
include plugin_dir_path(__FILE__) . '/settings.php';

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_info_box_load_textdomain'
);

function antique_info_box_load_textdomain() {
    load_plugin_textdomain(
            domain: 'antique-info-box',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

function antique_info_box_translation_dummy() {
    $plugin_description = __('Provide additional information about your '
            . 'article by inserting an information box.',
            'antique-info-box');
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'antique_info_box_settings_link'
);

function antique_info_box_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=antique_info_box');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'antique-info-box')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_head',
        callback: 'antique_info_box_custom_style'
);

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_info_box_custom_style'
);

function antique_info_box_custom_style() {

    $options = get_antique_info_box_options();

    $heading_has_border = $options['heading_border_bool'] == 'on';
    $content_has_border = $options['content_border_bool'] == 'on';
    $is_antique = $options['antique_style'] == 'on' && wp_get_theme() == 'Antique';

    $heading_bg_color = $is_antique ? 'var(--antique-blocks--heading--bg-color)' :
            $options['heading_bg_color'];
    $heading_font_color = $is_antique ? 'var(--antique-blocks--heading--font-color)' :
            $options['heading_font_color'];
    $heading_border_color = $is_antique ? 'var(--antique-blocks--heading--border-color)' :
            $options['heading_border_color'];
    $heading_border = $heading_has_border ? sprintf(
                    '%spx %s %s',
                    $options['heading_border_width'],
                    $options['heading_border_style'],
                    $heading_border_color
            ) : 'none';

    $content_bg_color = $is_antique ? 'var(--antique-blocks--content--bg-color)' :
            $options['content_bg_color'];
    $content_font_color = $is_antique ? 'var(--antique-blocks--content--font-color)' :
            $options['content_font_color'];
    $content_border_color = $is_antique ? 'var(--antique-blocks--content--border-color)' :
            $options['content_border_color'];
    $content_border = $content_has_border ? sprintf(
                    '%spx %s %s',
                    $options['content_border_width'],
                    $options['content_border_style'],
                    $content_border_color
            ) : 'none';

    $style = sprintf(
            '<style id="antique-info-box-custom-css">'
            . ':root {'
            . '--antique-info-box--heading--bg-color: %s;'
            . '--antique-info-box--heading--font-color: %s;'
            . '--antique-info-box--heading--border: %s;'
            . '--antique-info-box--content--bg-color: %s;'
            . '--antique-info-box--content--font-color: %s;'
            . '--antique-info-box--content--border: %s;'
            . '}'
            . '</style>',
            $heading_bg_color,
            $heading_font_color,
            $heading_border,
            $content_bg_color,
            $content_font_color,
            $content_border
    );

    echo $style;
}

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_info_box_meta_style'
);

function antique_info_box_meta_style() {

    $handle = 'antique-info-box-meta-style';

    wp_register_style(
            handle: $handle,
            src: plugins_url('/assets/css/admin.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_enqueue_style(
            handle: $handle
    );
}

/*
  ----------------------------------------
  Info Box
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_info_box_register_block'
);

function antique_info_box_register_block() {

    $asset_file = include( plugin_dir_path(__FILE__) . '/block/block.asset.php');
    $script_handle = 'antique-info-box-block-script';
    
    wp_register_script(
            handle: $script_handle,
            src: plugins_url('block/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );
    
    wp_set_script_translations(
            handle: $script_handle,
            domain: 'antique-info-box',
            path: plugin_dir_path(__FILE__) . '/languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/block/',
            args: array(
                'api_version' => 2,
                'render_callback' => 'antique_info_box_render_callback'
            )
    );
}

function antique_info_box_render_callback($block_attributes, $content) {
    return get_the_antique_info_box();
}

function get_the_antique_info_box() {

    $options = get_antique_info_box_options();
    $is_closed = $options['on_large_screen'] == 'closed';

    $post_meta = antique_info_box_get_post_meta(
            id: get_the_ID()
    );

    $info_title = $post_meta['title'];
    $info_fields = $post_meta['fields'];

    if (count($info_fields) < 1) {
        return;
    }

    $info_box = '<div class="antique-info-box antique-block">'
            . '<div class="antique-info-box-heading-wrap antique-block-heading-wrap">'
            . '<span class="antique-info-box-heading antique-block-heading">'
            . $info_title
            . '</span>'
            . '<span class="antique-block-arrow antique-block-show-content'
            . ($is_closed ? ' rotate-arrow' : '')
            . '">'
            . '<i class="fa fa-angle-down"></i>'
            . '</span>'
            . '</div>'
            . '<div class="antique-info-box-data-wrap antique-block-content'
            . ($is_closed ? ' hide-content' : '')
            . '">';

    foreach ($info_fields as $field) {

        if (isset($field['subject']) || isset($field['description'])) {

            $info_box .= '<div class="antique-info-box-data">'
                    . '<span>' . $field['subject'] . '</span>'
                    . '<span>' . $field['description'] . '</span>'
                    . '</div>';
        }
    }

    $info_box .= '</div>'
            . '</div>';

    return $info_box;
}

/*
  ----------------------------------------
  Meta Boxes
  ----------------------------------------
 */

function antique_info_box_get_post_meta($id) {

    $defaults = antique_info_box_get_meta_defaults();

    $meta_key = antique_info_box_get_meta_strings()['key'];
    $saved_meta = get_post_meta(
            post_id: $id,
            key: $meta_key,
            single: false
    );

    $full_meta = antique_plugins_parse_meta_args(
            args: isset($saved_meta[0]) ? $saved_meta[0] : array(),
            defaults: $defaults
    );

    return $full_meta;
}

function antique_info_box_get_meta_defaults() {

    $defaults = array(
        'title' => esc_html__('Info box', 'antique-info-box'),
        'fields' => array()
    );

    return $defaults;
}

function antique_info_box_get_meta_strings() {

    $meta_strings = array(
        'field' => 'antique_info_box_meta_field',
        'key' => '_antique_info_box_meta_key',
        'nonce_name' => 'antique_info_box_meta_nonce',
        'nonce_action' => 'add_antique_info_box_meta_nonce',
    );

    return $meta_strings;
}

add_action(
        hook_name: 'add_meta_boxes',
        callback: 'antique_info_box_meta_box'
);

function antique_info_box_meta_box() {

    add_meta_box(
            id: 'antique_info_box',
            title: __('Info box', 'antique-info-box'),
            callback: 'antique_info_box_meta_box_html',
            screen: array(
                'page',
                'post'
            ),
            context: 'advanced',
            priority: 'high'
    );
}

function antique_info_box_meta_box_html($post) {

    $meta = antique_info_box_get_meta_strings();

    $meta_field = $meta['field'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    wp_nonce_field(
            action: $nonce_action,
            name: $nonce_name
    );

    $post_meta = antique_info_box_get_post_meta(
            id: $post->ID
    );

    $info_title = $post_meta['title'];
    $info_fields = $post_meta['fields'];
    ?>

    <div class="antique-info-box-as-meta-box">
        <div class="antique-info-box-meta-row antique-info-box-meta-labels">
            <div>
                <span><?php echo esc_html_e('Title:', 'antique-info-box'); ?></span>
            </div>
        </div>
        <div class="antique-info-box-meta-row antique-info-box-meta-data">
            <div>
                <textarea name="<?php esc_attr_e($meta_field); ?>[title]"
                          rows="1"
                          autocomplete="off"
                          /><?php echo $info_title; ?></textarea>
            </div>
        </div>
        <div class="antique-info-box-meta-row antique-info-box-meta-labels">
            <div>
                <span>
                    <?php esc_html_e('Heading:', 'antique-info-box'); ?>
                </span>
            </div>
            <div>
                <span>
                    <?php esc_html_e('Description:', 'antique-info-box'); ?>
                </span>
            </div>
        </div>

        <?php
        $counter = 0;

        if (count($info_fields) > 0) {
            foreach ($info_fields as $field) {

                // Is this "dangerous"? Problem is that empty fields are
                // saved, but only removed if one updates the page editor
                // a second time.
                // if (isset($field['subject']) || isset($field['description'])) {
                if ($field['subject'] != '' || $field['description'] != '') {
                    ?>

                    <div class="antique-info-box-meta-row antique-info-box-meta-data">
                        <div>
                            <textarea name="<?php esc_attr_e($meta_field); ?>[fields][<?php echo esc_attr($counter); ?>][subject]"
                                      rows="1"
                                      autocomplete="off"
                                      /><?php echo $field['subject']; ?></textarea>
                        </div>
                        <div>
                            <textarea name="<?php esc_attr_e($meta_field); ?>[fields][<?php echo esc_attr($counter); ?>][description]"
                                      rows="1"
                                      autocomplete="off"><?php echo $field['description']; ?></textarea>
                        </div>
                        <button type="button" class="antique-info-box-meta-remove">
                            <?php esc_html_e('Remove', 'antique-info-box'); ?>
                        </button>
                    </div>

                    <?php
                    $counter += 1;
                }
            }
        }
        ?>

        <script>
            var $ = jQuery.noConflict();

            $(document).ready(function () {

                var count = <?php echo $counter; ?>;

                $('.antique-info-box-meta-add').click(function () {

                    $('.antique-info-box-as-meta-box').append(
                            '<div class="antique-info-box-meta-row antique-info-box-meta-data">'
                            + '<div>'
                            + '<textarea '
                            + 'name="<?php esc_attr_e($meta_field); ?>[fields][' + count + '][subject]" '
                            + 'rows="1" '
                            + 'autocomplete="off"/>'
                            + '</textarea>'
                            + '</div>'
                            + '<div>'
                            + '<textarea '
                            + 'name="<?php esc_attr_e($meta_field); ?>[fields][' + count + '][description]" '
                            + 'rows="1" '
                            + 'autocomplete="off">'
                            + '</textarea>'
                            + '</div>'
                            + '<div>'
                            + '<button type="button" class="antique-info-box-meta-remove">'
                            + '<?php esc_html_e('Remove', 'antique-info-box'); ?>'
                            + '</button>'
                            + '</div>'
                            + '</div>'
                            );

                    count = count + 1;
                });

                $(document).on('click', '.antique-info-box-meta-remove', function () {
                    $(this).closest('.antique-info-box-meta-data').remove();
                });

            });
        </script>
    </div>

    <div style="display: flex; flex-direction: row;">
        <button type="button" class="antique-info-box-meta-add">
            <?php esc_html_e('Add', 'antique-info-box'); ?>
        </button>
    </div>

    <?php
}

add_action(
        hook_name: 'save_post',
        callback: 'antique_info_box_save_meta'
);

function antique_info_box_save_meta($post_id) {

    $meta = antique_info_box_get_meta_strings();

    $meta_field = $meta['field'];
    $meta_key = $meta['key'];

    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    if (!isset($_POST[$nonce_name])) {
        return;
    }

    if (!wp_verify_nonce(
                    nonce: $_POST[$nonce_name],
                    action: $nonce_action)
    ) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (
            isset($_POST['post_type']) && (
            'page' == $_POST['post_type'] || 'post' == $_POST['post_type']
            )
    ) {
        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    }

    if (!isset($_POST[$meta_field])) {
        return;
    }

    $sanitized = antique_info_box_sanitize_meta(
            post_meta: $_POST[$meta_field]
    );

    update_post_meta(
            post_id: $post_id,
            meta_key: $meta_key,
            meta_value: $sanitized
    );
}

function antique_info_box_sanitize_meta($post_meta) {

    $allowed_html = array(
        'a' => array(
            'href' => array(),
            'target' => array()
        ),
        'b' => array(),
        'strong' => array(),
        'em' => array(),
        'br' => array()
    );

    $defaults = antique_info_box_get_meta_defaults();
    $values = antique_plugins_parse_meta_args(
            args: $post_meta,
            defaults: $defaults
    );

    $sanitized = antique_plugins_sanitize_text_array($values, $allowed_html);

    return $sanitized;
}
