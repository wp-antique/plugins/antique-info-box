��    *      l      �      �     �     �     �  �   �     �  	   �     �     �     �     �     �     �     �                      %   '  R   M     �     �     �     �     �     �     �  �   �    �     �     �          $     ,     =     B     K     P     U     Z     b     h  �  m     �	     
     
  �   )
        	   2     <     I     U     ]     k     y  
   �     �     �     �     �  *   �  _   �  	   F     P     ^     t     �     �     �    �  F  �  *   �  '        G     S     Z     o     t     {     �     �     �     �     �   Add Antique Info Box Antique Plugins Apply the colors set within the Antique theme. This option only works if the Antique theme is installed and activated. Upon activation of another theme the colors specified above are used. Background color: Behaviour Block Content Block Title Border: Description: Font color: Heading: Hide ID Info Box Info box Large screen: Pages and posts containing the block: Provide additional information about your article by inserting an information box. Remove Reset Save changes Settings Show Small screen: Title: When you create or edit a page, scroll down to the end of the page. There you find a settings box where you can set the box title and add (or remove) fields, which allow you to display a heading and a corresponding description. You can display the info box by selecting the block "Antique Info Box" and inserting it into the page. Additionally, if you are using the Antique theme, there is a "Sidebar" section in the right settings sidebar. There you can choose to display the info box in the sidebar of the page. Your settings have been reset. Your settings have been saved. closed content content, sidebar link location open page post sidebar title type Project-Id-Version: Antique Info Box
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Simon Garbin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2023-05-26 15:17+0200
PO-Revision-Date: 2023-05-26 15:17+0200
Language: de_DE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e
X-Poedit-SearchPath-0: settings.php
X-Poedit-SearchPath-1: antique-info-box.php
X-Poedit-SearchPath-2: block/block.js
X-Poedit-SearchPath-3: block/block.json
X-Poedit-SearchPath-4: shared/menu.php
 Hinzufügen Antique Info-Box Antique Plugins Verwende die Farben, die im Antique-Theme festgelegt wurden. Diese Einstellung kann nur angewandt werden, wenn das Antique-Theme installiert und aktiviert ist. Sobald du ein anderes Theme aktivierst, werden die oben festgelegten Farben verwendet. Hintergrundfarbe: Verhalten Block-Inhalt Block-Titel Rahmen: Beschreibung: Schriftfarbe: Überschrift: Verstecken ID Info-Box Info-Box Großer Bildschirm: Seiten und Posts, die den Block enthalten: Stelle deinen Lesern mit einer Info-Box zusätzliche Informationen über deinen Artikel bereit. Entfernen Zurücksetzen Änderungen speichern Einstellungen Anzeigen Kleiner Bildschirm: Titel: Wenn du eine Seite erstellst oder bearbeitest, scrolle runter zum Seitenende. Dort kannst du in den Einstellungen den Titel der Info-Box festlegen und Felder hinzufügen (oder entfernen), die aus einer Überschrift und einer dazugehörigen Beschreibung bestehen. Um die Info-Box anzuzeigen, wähle den Block "Antique Info-Box" und füge ihn in deine Seite ein. Zusätzlich siehst du, falls das Antique-Theme aktiviert ist, in der rechten Seitenleiste unter den Einstellungen den Abschnitt "Seitenleiste". Dort kannst du festlegen, ob die Info-Box in der Seitenleiste angezeigt werden soll. Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. geschlossen Inhalt Inhalt, Seitenleiste Link Stelle offen Seite Post Seitenleiste Titel Typ 